import React, { useRef, useState, useEffect } from "react";
import { View } from "../../components/Themed";
import styles from "./styles";
// import { Episode } from "../../types";
import { Video } from "expo-av";
import { Playback } from "expo-av/build/AV";
import { Storage } from "aws-amplify";
import { Episode } from "../../src/models";

interface VideoPlayerProps {
  episode: Episode;
}

const VideoPlayer = (props: VideoPlayerProps) => {
  const { episode } = props;
  const [status, setStatus] = useState({});

  const [videoUrl, setVideoUrl] = useState('');

  const video = useRef<Playback>(null);

  useEffect(()=> {
    if(episode.video.startsWith('http')){
      setVideoUrl(episode.video)
      return;
    }
    Storage.get(episode?.video).then(setVideoUrl)
  }, [episode])

  useEffect(()=> {
      if(!video) {
          return;
      }
    //   when another episode is clicked, the first video unloads and the second video begins
      (async ()=> {
          await video?.current?.unloadAsync();
          await video?.current?.loadAsync({
              uri: videoUrl
          }, {}, false)
      })();
  }, [videoUrl])

  if(videoUrl === '') {
    return null;
  }

  return (
    <View>
      <Video
        ref={video}
        style={styles.video}
        source={{
          uri: videoUrl,
        }}
        posterStyle={{resizeMode: "cover"}}
        posterSource={{uri: episode.poster}}
        usePoster={false}
        useNativeControls
        resizeMode="contain"
        onPlaybackStatusUpdate={(status) => setStatus(() => status)}
      />
    </View>
  );
};

export default VideoPlayer;
