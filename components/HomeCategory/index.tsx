import * as React from "react";
import { Text } from "../../components/Themed";
import { FlatList, View } from "react-native";
import styles from "./styles";

import { Category, Movie } from "../../src/models/index";
import { DataStore } from "aws-amplify";
import MovieItem from '../MovieItem/index'

interface HomeCategoryProps {
  category: Category;
}

const HomeCategory = (props: HomeCategoryProps) => {
  const { category } = props;

  const [movies, setMovies] = React.useState<Movie[]>([]);


  React.useEffect(() => {
    const fetchMovies = async () => {
      const data = (await DataStore.query(Movie)).filter(
        (movie) => movie.categoryID === category.id
      );
      setMovies(data);
    };
    fetchMovies();
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{category.title}</Text>
      <FlatList
        data={movies}
        renderItem={({ item }) => <MovieItem movie={item} />}
        horizontal
        showsHorizontalScrollIndicator={false}
      />
    </View>
  );
};

export default HomeCategory;




// FIXME: FORMER CODE BEFORE TRYING TO GET IMAGES FROM THE S3 BUCKET

// import * as React from "react";
// import { Text } from "../../components/Themed";
// import { Image, FlatList, View, Pressable } from "react-native";
// import styles from "./styles";
// import { useNavigation } from "@react-navigation/native";
// import { Category, Movie } from "../../src/models/index";
// import { DataStore, Storage } from "aws-amplify";

// interface HomeCategoryProps {
//   category: Category;
// }

// const HomeCategory = (props: HomeCategoryProps) => {
//   const { category } = props;

//   const [movies, setMovies] = React.useState<Movie[]>([]);

//   const navigation = useNavigation();

//   const onMoviePress = (movie: Movie) => {
//     navigation.navigate("MovieDetailScreen", { id: movie.id });
//   };

//   React.useEffect(() => {
//     const fetchMovies = async () => {
//       const data = (await DataStore.query(Movie)).filter(
//         (movie) => movie.categoryID === category.id
//       );
//       setMovies(data);
//     };
//     fetchMovies();
//   }, []);

//   Storage.get('profile-image.png')
//   .then(result => console.log(result))
//   return (
//     <View style={styles.container}>
//       <Text style={styles.title}>{category.title}</Text>
//       <FlatList
//         data={movies}
//         renderItem={({ item }) => (
//           <Pressable onPress={() => onMoviePress(item)}>
//             <Image style={styles.image} source={{ uri: item.poster }} />
//           </Pressable>
//         )}
//         horizontal
//         showsHorizontalScrollIndicator={false}
//       />
//     </View>
//   );
// };

// export default HomeCategory;
