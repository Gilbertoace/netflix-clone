import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        marginBottom: 10
    },
      image: {
          width: 100,
          height: 170,
          resizeMode: 'cover',
          borderRadius: 5,
          margin: 5
      },
      title: {
          fontSize: 18,
          fontWeight: 'bold',
          color: '#ccc'
      }
})

export default styles;