import React from "react";
import { Text, View } from "../../components/Themed";
import styles from "./styles";
import { Image, Pressable } from "react-native";
import { AntDesign } from '@expo/vector-icons';
import { Episode } from "../../src/models";
// import { Episode } from "../../types";

// Episode is used in another component, therefore was sourced from an external file called types.tsx
interface EpisodeItemProps {
  episode: Episode;
  onPress: (episode: Episode) => {}
}

const EpisodeItem = (props: EpisodeItemProps) => {
  const { episode, onPress } = props;
  return (
    <Pressable style={{ backgroundColor: "#000", margin: 10 }} onPress={()=> onPress(episode)}>
      <View style={{ ...styles.row, backgroundColor: "#000" }}>
        <Image style={styles.image} source={{ uri: episode.poster }} />
        <View style={{...styles.titleContainer, backgroundColor: "#000" }}>
          <Text style={styles.title}>{episode.title}</Text>
          <Text style={styles.duration}>{episode.duration}</Text>
        </View>

        <AntDesign name='download' size={24} color='#ccc' />
      </View>

  <Text style={styles.plot}>{episode.plot}</Text>
    </Pressable>
  );
};

export default EpisodeItem;
