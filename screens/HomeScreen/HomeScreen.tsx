import * as React from 'react';
import { View } from '../../components/Themed';
import styles from './style';
import { DataStore } from 'aws-amplify';

// import categories from '../../assets/data/categories';

import HomeCategory from '../../components/HomeCategory/index'
import { FlatList } from 'react-native';
import { Category } from '../../src/models/index';

const HomeScreen = () => {
  const [categories, setCategories] = React.useState<Category[]>([]);

  React.useEffect(()=> {
    const fetchCategories = async () => {
      try {
        let data = await DataStore.query(Category);
        setCategories(data);
      } catch (error) {
        if(error) {
          console.log("Error in fetching categories", error);
        }
      }
    }
    fetchCategories();
  }, []);
  return (
    <View style={styles.container}>
      <FlatList 
      data={categories}
      renderItem={({item})=> (
        <HomeCategory category={item} />
      )}
      />
    </View>
  );
};

export default HomeScreen;