import React, { useState, useEffect } from "react";
import { View, Text } from "../../components/Themed";
import styles from "./style";
import {
  MaterialIcons,
  Entypo,
  AntDesign,
  Feather,
  FontAwesome,
} from "@expo/vector-icons";
import { DataStore } from "aws-amplify";
import { Movie, Season, Episode } from "../../src/models/index";

// import movie from "../../assets/data/movie";
import { Image, Pressable, FlatList, ActivityIndicator } from "react-native";
import EpisodeItem from "../../components/EpisodeItem";
import { Picker } from "@react-native-picker/picker";
import VideoPlayer from "../../components/VideoPlayer";
import { useRoute } from "@react-navigation/native";

// const firstSeason = movie.seasons.items[0];
// const firstEpisode = firstSeason.episodes.items[0];

const MovieDetailScreen = () => {
  const [movie, setMovie] = useState<Movie | undefined>(undefined);
  const [seasons, setSeason] = useState<Season[]>([]);
  const [episodes, setEpisode] = useState<Episode[]>([]);

  const [currentSeason, setCurrentSeason] = useState<Season|undefined>(undefined);
  //used for VideoPlayer component
  const [currentEpisode, setCurrentEpisode] = useState<Episode|undefined>(undefined);

  // movies id is gotten from the previous route--- the onMoviePress function in HomeCategoryScreen.tsx
  // it displays data via the movie id hence the route.params.id
  const route = useRoute();

  // FETCH MOVIES USING PARAMS(id) PASSED FROM HOMECATEGORYSCREEN WHEN A MOVIE
  useEffect(() => {
    const fetchMovie = async () => {
        const data = await DataStore.query(Movie, route?.params?.id);
        setMovie(data);
    };
    fetchMovie();
  }, []);

  // FETCH SEASONS
  useEffect(() => {
    if(!movie) {
      return;
    }
    const fetchSeasons = async () => {
        const movieSeasons = (await DataStore.query(Season)).filter(
          (season) => season?.movie?.id === movie.id
        );
        console.log(movieSeasons);
        setSeason(movieSeasons);
        setCurrentSeason(movieSeasons[0]);
    };
    fetchSeasons();
  }, [movie]);


  // FETCH EPISODES
  useEffect(()=> {
    if(!currentSeason) {
      return;
    }

    const fetchEpisodes = async () => {
      const data = (await DataStore.query(Episode)).filter(episode => episode?.season?.id === currentSeason?.id)
      // console.log(data);
      setEpisode(data);
      setCurrentEpisode(data[0]);
    }
    fetchEpisodes();
  }, [currentSeason])

  const seasonNames = seasons ? seasons.map(season => season.name) : [];

  if (!movie) {
    return <ActivityIndicator />;
  }

  return (
    <View style={styles.container}>
      {currentEpisode && <VideoPlayer episode={currentEpisode} />}
      <FlatList
        data={episodes}
        renderItem={({ item }) => (
          <EpisodeItem episode={item} onPress={setCurrentEpisode} />
        )}
        // style={{ marginBottom: 250 }}
        ListHeaderComponent={
          <View style={{ padding: 12, backgroundColor: "#000" }}>
            <Text style={styles.title}>{movie.title}</Text>
            <View style={{ flexDirection: "row", backgroundColor: "#000" }}>
              <Text style={styles.match}>98% match</Text>
              <Text style={styles.year}>{movie.year}</Text>
              <View style={styles.ageContainer}>
                <Text style={styles.age}>12+</Text>
              </View>
              <Text style={styles.year}>
                {movie.numberOfSeasons} Season
                {movie.numberOfSeasons > 1 ? "s" : ""}
              </Text>
              <MaterialIcons name="hd" size={24} color="#fff" />
            </View>

            {/* Play Btn */}
            <Pressable
              onPress={() => {
                console.log("Play Button Pressed");
              }}
              style={styles.playBtn}
            >
              <Text style={styles.playBtnText}>
                <Entypo name="controller-play" size={17} color="black" /> Play
              </Text>
            </Pressable>

            {/* Download Btn */}
            <Pressable
              onPress={() => {
                console.log("Download Button Pressed");
              }}
              style={styles.downloadBtn}
            >
              <Text style={styles.downloadBtnText}>
                <AntDesign name="download" size={16} color="#fff" /> Download
              </Text>
            </Pressable>

            <Text style={{ color: "#ccc", marginVertical: 10 }}>
              {movie.plot}
            </Text>
            <Text style={styles.year}>Cast: {movie.cast}</Text>
            <Text style={styles.year}>Creator: {movie.creator}</Text>

            <View
              style={{
                flexDirection: "row",
                backgroundColor: "#000",
                marginTop: 20,
              }}
            >
              <View
                style={{
                  alignItems: "center",
                  backgroundColor: "#000",
                  marginHorizontal: 20,
                }}
              >
                <AntDesign name="plus" size={24} color="#ccc" />
                <Text
                  style={{ ...styles.text, color: "darkgrey", marginTop: 5 }}
                >
                  My List
                </Text>
              </View>

              <View
                style={{
                  alignItems: "center",
                  backgroundColor: "#000",
                  marginHorizontal: 20,
                }}
              >
                <Feather name="thumbs-up" size={24} color="#ccc" />
                <Text
                  style={{ ...styles.text, color: "darkgrey", marginTop: 5 }}
                >
                  Rate
                </Text>
              </View>

              <View
                style={{
                  alignItems: "center",
                  backgroundColor: "#000",
                  marginHorizontal: 20,
                }}
              >
                <FontAwesome name="send-o" size={24} color="#ccc" />
                <Text
                  style={{ ...styles.text, color: "darkgrey", marginTop: 5 }}
                >
                  Share
                </Text>
              </View>
            </View>

            {currentSeason && (
              <Picker
                selectedValue={currentSeason.name}
                onValueChange={(itemValue, itemIndex) =>
                  setCurrentSeason(seasons[itemIndex])
                }
                style={{ color: "#fff", marginTop: 10, width: 130 }}
                dropdownIconColor="#fff"
              >
                {seasonNames.map((seasonName) => (
                  <Picker.Item
                    key={seasonName}
                    label={seasonName}
                    value={seasonName}
                  />
                ))}
              </Picker>
            )}
          </View>
        }
      />
    </View>
  );
};

export default MovieDetailScreen;
