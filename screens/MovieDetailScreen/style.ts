import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000',
    },
    image: {
        width: '100%',
        aspectRatio: 16/9,
        resizeMode: 'cover'
    },
    text: {
        color: '#ccc'
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#ccc',
        lineHeight: 35
    },
    match: {
        color: '#59d467',
        fontWeight: 'bold',
        marginRight: 5
    },
    year: {
        color: '#757575',
        marginRight: 5,
    },
    ageContainer: {
        backgroundColor: '#e6e229',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2,
        paddingHorizontal: 3,
        marginRight: 5,
    },
    age: {
        color: 'black',
        fontWeight: 'bold'
    },

    // Buttons
    playBtn: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 5,
        borderRadius: 3,
        marginVertical: 5
    },
    playBtnText: {
        color: 'black',
        fontSize: 16,
        fontWeight: 'bold'
    },
    downloadBtn: {
        backgroundColor: '#2b2b2b',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 5,
        borderRadius: 3,
        marginVertical: 5
    },
    downloadBtnText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 16
    }
})

export default styles;